///hello_world-1.3:games
/// [!-]-[!a-z,!!@#$%^&*()
/// no spaces anywhere
///
///valid: [ all lowercase ascii characters besides :-]-[numbers and periods]:[lowercase ascii characters]
///I will cross the ascii enforcement bridge when I come to it


pub struct Lexer {
    state: State,
    text: Vec<u8>,
    bot: usize,
    top: usize,
    eof: bool,
    pkgstr: PkgStr,
}

#[derive(Clone,Debug)]
pub struct PkgStr {
    name: String,
    version: Option<String>,
    category: Option<String>,

}

impl Lexer {
    fn advance(&mut self, c: u8) {
        match self.state {
            State::Name => {
                match c {
                    //dash
                    45 => {
                        self.extract();
                        self.state = State::Version;
                    },
                    //colon
                    58 => {
                        self.extract();
                        self.state = State::Category;
                    },
                    _ => {},
                }
            },
            State::Version => {
                match c {
                    //colon
                    58 => {
                        self.extract();
                        self.state = State::Category;
                    },
                    _ => {},
                }
            },
            State::Category => {
            }
            State::Finished => {
                unreachable!()
            }
        }
    }
    pub fn lex(&mut self) -> PkgStr {
        while self.state != State::Finished {
            if let Some(c) = self.current() {
                self.advance(c)
            } else {
                self.extract();
                self.state = State::Finished;
            }
        }
        assert_eq!(self.state,State::Finished);
        self.pkgstr.clone()
    }
    fn current(&mut self) -> Option<u8> {
        let mut ret = None;
        if self.text.len()-1 <= self.top {
        } else {
            ret = Some(self.text[self.top]);
        }
        self.top +=1;
        ret
    }
    fn extract(&mut self) {
        if self.eof {
            self.top+=1;
            self.text.push(49);
        }
        self.top-=1;
        //this is safe I promise, we checked if this is ascii earlier
        let s = unsafe {
            String::from_utf8_unchecked(self.text[self.bot .. self.top].to_vec())
        };
        match self.state {
            State::Name => self.pkgstr.name = s,
            State::Version => self.pkgstr.version = Some(s),
            State::Category => self.pkgstr.category = Some(s),
            State::Finished => unreachable!(),
        }
        //skip the dedchar
        self.top+=1;
        self.bot = self.top;
    }
}

impl From<String> for Lexer {
    fn from(s: String) -> Self {
        assert!(s.is_ascii());
        assert_eq!(s.to_ascii_lowercase(), s);
        Lexer {
            state: State::Name,
            text: s.into_bytes(),
            bot: 0,
            top: 0,
            eof: false,
            pkgstr: PkgStr {
                name: String::new(),
                version: None,
                category: None,
            },
        }
    }
}

#[derive(PartialEq,Eq,Debug)]
enum State {
    Name,
    Version,
    Category,
    Finished,
}
