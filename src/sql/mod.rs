extern crate rusqlite;
use self::rusqlite::Connection;

struct Pdbentry {
    version: String,
    name: String,
    summary: String,
    homepage: String,
    binaries: Vec<File>,
    sos: Vec<File>,
    slibs: Vec<File>,
    deps: Vec<Pdbentry>,
    category: String,
    src: String,
    digest: String,
    configs: Vec<File>,
}
struct File {
    Path: String,
}
struct Pdb {
    conn: Connection,
}
impl Pdb {
    fn open() -> Self {
        Pdb {
            conn: Connection::open("./pdb.db").unwrap(),
        }
    }
     
}
enum InstallBy {
    GroupNameVersion(String,String,String),
    NameVersion(String,String),
    Name(String),
}

struct SearchBy {
    version: bool,
    name: bool,
    summary: bool,
    homepage: bool,
    binaries: bool,
    sos: bool,
    slibs: bool,
    category: bool,
    configs: bool,
}

macro_rules! o {
    ($x:ident) => (
        pub fn $x(&mut self, t:bool) {
            self.$x=t
        }
    )
}
impl SearchBy {
    o!(version);
    o!(name);
    o!(summary);
    o!(homepage);
    o!(binaries);
    o!(sos);
    o!(slibs);
    o!(category);
    o!(configs);
}
