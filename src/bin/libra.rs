extern crate rusqlite;
extern crate libra;


use libra::pkgstr::Lexer;
use libra::sql::Pdb;


fn main() {
    //do in paralol
    println!("Hello, world!");
    println!("{:?}",Lexer::from(String::from("hello_world-1.2.3:games")).lex());
    println!("{:?}",Lexer::from(String::from("hello_world-1.2.3")).lex());
    println!("{:?}",Lexer::from(String::from("hello_world:games")).lex());
    println!("{:?}",Lexer::from(String::from("hello_world")).lex());
    
}
